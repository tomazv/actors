using System;

namespace Actors
{
    public class ActorSystem
    {
        private readonly Func<Type, Actor> _createActor;

        public ActorSystem() : this(t => (Actor)Activator.CreateInstance(t))
        {
        }

        public ActorSystem(Func<Type, Actor> createActor)
        {
            _createActor = createActor;
        }

        public ActorRef ActorOf<TActor>(params object[] args) where TActor : Actor
        {
            return new ActorRef(() => _createActor(typeof(TActor)), args ?? new object[0]);
        }
    }
}