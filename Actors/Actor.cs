using System.Threading.Tasks;

namespace Actors
{
    public abstract class Actor
    {
        protected static readonly Task CompletedTask = Task.FromResult(true);

        protected ActorRef Self { get; private set; }

        internal void InitializeInternal(ActorRef self)
        {
            Self = self;
        }

        protected virtual internal Task InitializeAsync(object[] args)
        {
            return CompletedTask;
        }

        protected virtual internal Task StopAsync()
        {
            return CompletedTask;
        }

        abstract protected internal Task HandleAsync(object message, ActorRef sender);
    }
}