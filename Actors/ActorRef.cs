﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace Actors
{
    public class ActorRef
    {
        private readonly Func<Actor> _createActor;
        private readonly object[] _args;
        private readonly ActionBlock<Envelope> _inbox;
        private Actor _actor;
        private bool _stopped;
        private readonly object _syncRoot = new object();
        private readonly CancellationTokenSource _cancellationTokenSource;

        public ActorRef(Func<Actor> createActor, object[] args)
        {
            _createActor = createActor;
            _args = args;
            _cancellationTokenSource = new CancellationTokenSource();
            _inbox = new ActionBlock<Envelope>(e => HandleMessageAsync(e), new ExecutionDataflowBlockOptions 
            { 
                CancellationToken =  _cancellationTokenSource.Token
            });
        }

        public void Tell(object message)
        {
            Tell(message, null);
        }

        public void Tell(object message, ActorRef sender)
        {
            lock (_syncRoot)
            {
                if (_stopped)
                    return;
                _inbox.Post(new Envelope(message, sender));
            }
        }

        public async Task StopAsync(bool waitForAllMessages)
        {
            lock (_syncRoot)
            {
                if (_stopped)
                    return;
                _stopped = true;
            }
            if (!waitForAllMessages)
                _cancellationTokenSource.Cancel();
            _inbox.Complete();
            try
            {
                await _inbox.Completion;
            }
            catch (OperationCanceledException)
            {
            }
            await StopActorAsync();
        }

        private async Task HandleMessageAsync(Envelope envelope)
        {
            bool exceptionThrown = false;
            try
            {
                if (_actor == null)
                {
                    _actor = _createActor();
                    _actor.InitializeInternal(this);
                    await _actor.InitializeAsync(_args);
                }
                await _actor.HandleAsync(envelope.Message, envelope.Sender);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                exceptionThrown = true;
            }
            if (exceptionThrown)
                await StopActorAsync();
        }

        private async Task StopActorAsync()
        {
            if (_actor == null)
                return;
            try
            {
                try
                {
                    await _actor.StopAsync();
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                _actor = null;
            }
        }
    }
}
