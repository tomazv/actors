namespace Actors
{
    internal class Envelope
    {
        public object Message { get; private set; }
        public ActorRef Sender { get; private set; }

        public Envelope(object message, ActorRef sender)
        {
            Message = message;
            Sender = sender;
        }
    }
}