﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Actors.Specs
{
    public abstract class Spec
    {
        
        protected Spec()
        {
            this.Given();
            this.When();
        }

        public virtual void Given() { }
        public virtual void When() { }
    }
}
