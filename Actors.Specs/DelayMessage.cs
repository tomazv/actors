using System;

namespace Actors.Specs
{
    public class DelayMessage
    {
        public TimeSpan Delay { get; private set; }

        public DelayMessage(TimeSpan delay)
        {
            Delay = delay;
        }

    }
}