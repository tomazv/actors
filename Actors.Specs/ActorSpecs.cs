﻿using System;
using System.Threading;
using Xunit;
using Xunit.Should;

namespace Actors.Specs
{
    public class when_sending_multiple_messages_to_an_actor : ActorSpec
    {
        public when_sending_multiple_messages_to_an_actor()
        {
            var actorSystem = new ActorSystem();
            ActorRef actor = actorSystem.ActorOf<TestActor>();
            actor.Tell("Hello");
            actor.Tell("World");
            actor.StopAsync(true).Wait(TimeSpan.FromSeconds(1));
        }

        [Fact]
        public void it_should_process_messages_in_sequence()
        {
            Log.GetEntries().ShouldBe(new[] { "[1] Hello", "[2] World" });
        }
    }

    public class when_processing_a_message_throws_exception : ActorSpec
    {
        public when_processing_a_message_throws_exception()
        {
            var actorSystem = new ActorSystem();
            ActorRef actor = actorSystem.ActorOf<TestActor>();
            actor.Tell("Hello");
            actor.Tell(new ExceptionMessage(typeof(Exception)));
            actor.Tell("World");
            actor.StopAsync(true).Wait(TimeSpan.FromSeconds(1));
        }

        [Fact]
        public void it_should_restart_actor()
        {
            Log.GetEntries().ShouldBe(new[] { "[1] Hello", "[1] World" });
        }
    }

    public class when_stopping_an_actor_with_waiting_to_finish : ActorSpec
    {
        public when_stopping_an_actor_with_waiting_to_finish()
        {
            var actorSystem = new ActorSystem();
            ActorRef actor = actorSystem.ActorOf<TestActor>();
            actor.Tell("Hello");
            actor.Tell(new object[]
            {
                new DelayMessage(TimeSpan.FromMilliseconds(100)),
                "Delay"
            });
            actor.Tell("World");
            actor.StopAsync(true).Wait(TimeSpan.FromSeconds(1));
        }

        [Fact]
        public void it_should_wait_for_all_messages_to_be_processed_before_stopping()
        {
            Log.GetEntries().ShouldBe(new[] { "[1] Hello", "[2] Delay", "[3] World" });
        }
    }

    public class when_stopping_and_actor_without_waiting_to_finish : ActorSpec
    {
        public when_stopping_and_actor_without_waiting_to_finish()
        {
            var actorSystem = new ActorSystem();
            ActorRef actor = actorSystem.ActorOf<TestActor>();
            actor.Tell("Hello");
            actor.Tell(new object[]
            {
                new DelayMessage(TimeSpan.FromMilliseconds(100)),
                "Delay"
            });
            actor.Tell("World");
            Thread.Sleep(20);
            actor.StopAsync(false).Wait(TimeSpan.FromSeconds(1));
        }

        [Fact]
        public void it_should_wait_only_for_current_message_to_be_processed_before_stopping()
        {
            Log.GetEntries().ShouldBe(new[] { "[1] Hello", "[2] Delay" });
        }
    }

    public class when_actor_is_stopped : ActorSpec
    {
        public when_actor_is_stopped()
        {
            var actorSystem = new ActorSystem();
            ActorRef actor = actorSystem.ActorOf<TestActor>();
            actor.Tell("Hello");
            actor.StopAsync(true).Wait(TimeSpan.FromSeconds(1));
            actor.Tell("World");
            Thread.Sleep(20);
        }

        [Fact]
        public void it_should_ignore_all_messages()
        {
            Log.GetEntries().ShouldBe(new[] { "[1] Hello" });
        }
    }

    public class when_actor_stops_himself : ActorSpec
    {
        public when_actor_stops_himself()
        {
            var actorSystem = new ActorSystem();
            ActorRef actor = actorSystem.ActorOf<TestActor>();
            actor.Tell("Hello");
            actor.Tell(new PoisonMessage());
            actor.Tell("World");
            Thread.Sleep(20);
        }

        [Fact]
        public void it_should_ignore_all_messages()
        {
            Log.GetEntries().ShouldBe(new[] { "[1] Hello" });
        }
    }
}
