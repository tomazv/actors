using System.Collections.Generic;

namespace Actors.Specs
{
    public static class Log
    {
        private static readonly List<string> Entries = new List<string>();

        public static void Add(string format, params object[] args)
        {
            Entries.Add(string.Format(format, args));
        }

        public static void Clear()
        {
            Entries.Clear();
        }

        public static string[] GetEntries()
        {
            return Entries.ToArray();
        }
    }
}