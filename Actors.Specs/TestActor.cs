using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Actors.Specs
{
    public class TestActor : Actor
    {
        private int _index;

        protected override Task InitializeAsync(object[] args)
        {
            _index = 0;
            return CompletedTask;
        }

        protected override async Task HandleAsync(object message, ActorRef sender)
        {
            _index++;
            var messages = message as IEnumerable<object>;
            if (messages != null)
            {
                foreach (object actualMessage in messages)
                {
                    await HandleActualMessageAsync(actualMessage);
                }
            }
            else
            {
                await HandleActualMessageAsync(message);
            }
        }

        private async Task HandleActualMessageAsync(object message)
        {
            if (message is DelayMessage)
                await Task.Delay(((DelayMessage)message).Delay);
            else if (message is PoisonMessage)
                await Self.StopAsync(false);
            else if (message is ExceptionMessage)
                throw (Exception)Activator.CreateInstance(((ExceptionMessage)message).ExceptionType);
            else
                Log.Add("[{0}] {1}", _index, message);
        }
    }
}