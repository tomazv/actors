using System;

namespace Actors.Specs
{
    public class ExceptionMessage
    {
        public Type ExceptionType { get; private set; }

        public ExceptionMessage(Type exceptionType)
        {
            ExceptionType = exceptionType;
        }
    }
}